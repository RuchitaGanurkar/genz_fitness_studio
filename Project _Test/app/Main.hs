module Main where

import Web.Scotty
import Routes (appRoutes)


main :: IO ()
main = scotty 3000 
  appRoutes
